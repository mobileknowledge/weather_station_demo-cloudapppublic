﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherStationDemo_CloudContracts
{
    public class Measurement
    {
        public float Temperature { get; set; }
        public float Humidity { get; set; }
        public float Pressure { get; set; }
        public float Light { get; set; }
        public long Timestamp { get; set; }
    }
}
