﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WeatherStationDemo_CloudContracts;

namespace WeatherStationDemo_CloudApp.DataContext
{
    public class WeatherStationDatabaseContext : DbContext
    {
        public WeatherStationDatabaseContext()
            : base("WeatherStationDatabaseContext")
        {
        }

        //Add a table called devices_table in the Database. The table rows have the same objects as the RapidIoTDevice class
        public DbSet<RapidIoTDevice> devices_table { get; set; }
    }
}