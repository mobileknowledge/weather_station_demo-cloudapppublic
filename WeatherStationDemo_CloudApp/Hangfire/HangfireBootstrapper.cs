﻿using Hangfire;
using Hangfire.Storage.Monitoring;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Hosting;

namespace WeatherStationDemo_CloudApp
{
    //This is the generic class for the usage of Hangfire. This is a tool that makes the database calls more efficient as it implements
    //Background workers to avoid performing heavy operations in the main thread.
    public class HangfireBootstrapper : IRegisteredObject
    {
        public static readonly HangfireBootstrapper Instance = new HangfireBootstrapper();

        private readonly object _lockObject = new object();
        private bool _started;

        private BackgroundJobServer _backgroundJobServer;
        private BackgroundJobClient _backgroundJobClient;
        
        private HangfireBootstrapper()
        {
        }

        public void Start()
        {
            lock (_lockObject)
            {
                if (_started) return;
                _started = true;

                HostingEnvironment.RegisterObject(this);

                GlobalConfiguration.Configuration.UseSqlServerStorage("WeatherStationDatabaseContext");

                _backgroundJobServer = new BackgroundJobServer();
                _backgroundJobClient = new BackgroundJobClient();

                // Purge previously enqueued queues just in case jobs
                // PurgeAllQueues();
            }
        }

        public void Stop()
        {
            lock (_lockObject)
            {
                if (_backgroundJobServer != null)
                {
                    _backgroundJobServer.Dispose();
                }
                
                HostingEnvironment.UnregisterObject(this);
            }
        }

        void IRegisteredObject.Stop(bool immediate)
        {
            Stop();
        }

        void PurgeAllQueues()
        {
            var monitor = JobStorage.Current.GetMonitoringApi();
            foreach (QueueWithTopEnqueuedJobsDto queue in monitor.Queues())
            {
                PurgeQueue(queue.Name);
            }
        }

        void PurgeQueue(string queueName)
        {
            var toDelete = new List<string>();
            var monitor = JobStorage.Current.GetMonitoringApi();

            var queue = monitor.Queues().First(x => x.Name == queueName);
            for (var i = 0; i < Math.Ceiling(queue.Length / 1000d); i++)
            {
                monitor.EnqueuedJobs(queue.Name, 1000 * i, 1000)
                    .ForEach(x => toDelete.Add(x.Key));
            }
            foreach (var jobId in toDelete)
            {
                BackgroundJob.Delete(jobId);
            }
        }
    }
}