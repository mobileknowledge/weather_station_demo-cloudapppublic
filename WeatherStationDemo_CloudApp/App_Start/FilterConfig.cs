﻿using System.Web;
using System.Web.Mvc;

namespace WeatherStationDemo_CloudApp
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
