﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WeatherStationDemo_CloudApp.Definitions
{
    public class FileFormatDefinition
    {
        public static List<SelectListItem> listFormats = new List<SelectListItem>() {
                new SelectListItem(){ Value=".csv", Text=".csv"},
                new SelectListItem(){ Value=".txt", Text=".txt"} };
    }
}