﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WeatherStationDemo_CloudApp.ViewModels
{
    //The file format model contains the two possible export format for the files in the DataDisplayController (.csv, .txt).
    public class FileFormatModel
    {
        [Required]
        [Display(Name = "Export format:")]
        public string formats { get; set; }
    }
}