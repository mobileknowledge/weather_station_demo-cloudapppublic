﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WeatherStationDemo_CloudApp.ViewModels
{
    //The login view model will only have two required fields. BLE MAC Address and Session ID.
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Please enter the MAC Address")]
        [Display(Name = "Bluetooth MAC Address")]
        public string bleMACAddress { get; set; }

        [Required(ErrorMessage = "Please enter the session ID")]
        [Display(Name = "Session ID")]
        public string SessionID { get; set; }
    }
}