namespace WeatherStationDemo_CloudApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.RapidIoTDevices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        MACAddress = c.String(),
                        SessionID = c.String(),
                        Measurement_Temperature = c.Single(nullable: false),
                        Measurement_Humidity = c.Single(nullable: false),
                        Measurement_Pressure = c.Single(nullable: false),
                        Measurement_Light = c.Single(nullable: false),
                        Measurement_Timestamp = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.RapidIoTDevices");
        }
    }
}
