namespace WeatherStationDemo_CloudApp.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Rename_MACAddress_field : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.RapidIoTDevices", "bleMACAddress", c => c.String());
            DropColumn("dbo.RapidIoTDevices", "MACAddress");
        }
        
        public override void Down()
        {
            AddColumn("dbo.RapidIoTDevices", "MACAddress", c => c.String());
            DropColumn("dbo.RapidIoTDevices", "bleMACAddress");
        }
    }
}
