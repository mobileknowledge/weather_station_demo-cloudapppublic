﻿using System.Linq;
using System.Web.Mvc;
using WeatherStationDemo_CloudApp.DataContext;
using WeatherStationDemo_CloudApp.ViewModels;

namespace WeatherStationDemo_CloudApp.Controllers
{
    public class LoginController : Controller
    {
        //This is the application entry point. The user is redirected to the Index view.
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }
        
        //When hitting the Login button this HTTPPOST method is called with the user information for login.
        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(LoginViewModel modelLogin)
        {
            //Check if there is no missing fields in the login model (BLE MAC address and session ID)
            if (!ModelState.IsValid)
            {
                return View(modelLogin);
            }
            else
            {
                //Get database instance
                using (var DbContext = new WeatherStationDatabaseContext())
                {
                    //Check if there is any row that contains data for the session ID and MAC address provided.
                    var retrieveDeviceInfo = DbContext.devices_table.Where(device => device.SessionID == modelLogin.SessionID
                        && device.bleMACAddress.Substring(12) == modelLogin.bleMACAddress).FirstOrDefault();

                    //If there is data just redirecto to the DataDisplayController with the current session ID and MAC Address.
                    //Otherwise show an error in login
                    if (retrieveDeviceInfo != null)
                    {
                        return RedirectToAction("Main", "DataDisplay", new { sessionID = modelLogin.SessionID,
                            MACAddress = modelLogin.bleMACAddress
                        });
                    }
                    else
                    {
                        TempData["ErrorMessage"] = "Error in login, please check that the login data is correct.";
                        return View();
                    }
                }
            }
        }
    }
}