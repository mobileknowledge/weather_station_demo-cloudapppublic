﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using WeatherStationDemo_CloudApp.DataContext;
using WeatherStationDemo_CloudApp.Utils;
using WeatherStationDemo_CloudApp.ViewModels;
using WeatherStationDemo_CloudContracts;
using WeatherStationDemo_CloudApp.Definitions;

namespace WeatherStationDemo_CloudApp.Controllers
{
    //This is the main controller of the cloud application. It handles the data display in graphs, allows the user to download
    //measurements, allows to change from Celsius to Fahrenheit degrees and logout the current session.
    public class DataDisplayController : Controller
    {
        //Create a linked list to store the data for the sensors. Since we are storing the data using the same timestamp we can
        //use a common list containing the timestamp instead of a separate list for each measurement.
        static LinkedList<float> temperatureList = new LinkedList<float>();
        static LinkedList<long> timestampList = new LinkedList<long>();
        static LinkedList<float> humidityList = new LinkedList<float>();
        static LinkedList<float> pressureList = new LinkedList<float>();
        static LinkedList<float> lightList = new LinkedList<float>();

        //This is the entry point of the Controller when the user logs into the Cloud app, the session ID and MAC Address are stored as
        //cookies.
        public ActionResult Main(String sessionID, String MACAddress)
        {
            //Store the SessionID and MAC Address in cookies so they can be accessible from the Web client.
            Session["sessionID"] = sessionID;
            Session["MACAddress"] = MACAddress;

            //Pass the View a ViewBag variable containing the .csv and .txt formats to display the dropdownlist.
            ViewBag.Formats = FileFormatDefinition.listFormats.ToList();
            ViewBag.SessionID = sessionID;
            ViewBag.MACAddress = MACAddress;

            return View();
        }
        
        //The logout method deletes the current user data and returns to the Index page.
        public ActionResult Logout()
        {
            //Deletes the current user information, just store null value to make sure the previous value has dissapeard.
            Session["sessionID"] = null;
            Session["MACAddress"] = null;

            return RedirectToAction("Index", "Login");
        }

        //The UpdateCharts method is called from the AJAX Update() function in the Main.cshtml file. Returning a JsonResult object with 
        //the data retrieved from the database for a particular user. The timescale and temperature scale are shared so the controller
        //can process the data from the database according to these parameters introduced by the user.
        [HttpPost]
        public JsonResult UpdateCharts(string timescale, string tempscale)
        {
            //Get an instance of the database.
            using (var DbContext = new WeatherStationDatabaseContext())
            {
                //Read the sessionID and look in the database how many measurements there are with that particular Session ID.
                var mSessionID = Session["sessionID"];
                var databaseCount = DbContext.devices_table.Count(x => x.SessionID == mSessionID.ToString());

                //The default value for the temperature scale is Celsius, check if this setting has been changed.
                var myTempScale = tempscale;
                bool changeToFahrenheit = tempscale.Equals("Fahrenheit");

                //Check if there are elements for the current session ID
                if (databaseCount > 0)
                {
                    //Clear the data lists to avoid duplicate data
                    temperatureList.Clear();
                    humidityList.Clear();
                    pressureList.Clear();
                    lightList.Clear();
                    timestampList.Clear();

                    //Read last timestamp stored in the database.
                    var dbRow = DbContext.devices_table.Where(x => x.SessionID == mSessionID.ToString()).ToList().LastOrDefault();

                    //We need to keep track of the last timestamp and ID stored in the table as a reference to fill the Linked lists
                    //with the previous data.
                    var lastTimestamp = dbRow.Measurement.Timestamp;
                    var lastId = dbRow.Id;

                    //Add the Database row to the linked list.
                    temperatureList.AddFirst(convertTempIfNeeded(dbRow.Measurement.Temperature, changeToFahrenheit));
                    humidityList.AddFirst(dbRow.Measurement.Humidity);
                    pressureList.AddFirst(dbRow.Measurement.Pressure);
                    lightList.AddFirst(dbRow.Measurement.Light);
                    timestampList.AddFirst(lastTimestamp);

                    int index = 1;
                    int rowsInserted = 0; //Counts how many database rows have been included to show, limit to 30 samples to avoid
                                            //overload of samples in the graphs
                    while (index < databaseCount)
                    {
                        //Reads the next database row taking into account the last ID read from the previous row.
                        var nextDbRow = DbContext.devices_table.Where(x => x.Id < lastId && x.SessionID == mSessionID.ToString()).
                            OrderByDescending(x => x.Id).FirstOrDefault();
                        var nextTimestamp = nextDbRow.Measurement.Timestamp;

                        //Store the time difference between the two last measurments read, this is use afterwards to differenciate if the
                        //timestamp is 1 minute or 1 second so the measurement is not stored in the linked list until this condition is
                        //achieved.
                        TimeSpan difference = TimeConverter.UnixTimeStampToDateTime(nextTimestamp)
                            - TimeConverter.UnixTimeStampToDateTime(lastTimestamp);

                        switch (timescale)
                        {
                            case "Minutes":
                                if (difference.TotalMinutes <= -1) //If the time difference is 1 minute or more we can store it
                                {
                                    temperatureList.AddFirst(convertTempIfNeeded(nextDbRow.Measurement.Temperature, changeToFahrenheit));
                                    timestampList.AddFirst(nextTimestamp);
                                    humidityList.AddFirst(nextDbRow.Measurement.Humidity);
                                    pressureList.AddFirst(nextDbRow.Measurement.Pressure);
                                    lightList.AddFirst(nextDbRow.Measurement.Light);
                                    lastTimestamp = nextTimestamp;

                                    rowsInserted++;
                                }
                                break;
                            case "Seconds":
                                if (difference.TotalSeconds <= -1) //If the time difference is 1 second or more we can store it
                                {
                                    temperatureList.AddFirst(convertTempIfNeeded(nextDbRow.Measurement.Temperature, changeToFahrenheit));
                                    timestampList.AddFirst(nextTimestamp);
                                    humidityList.AddFirst(nextDbRow.Measurement.Humidity);
                                    pressureList.AddFirst(nextDbRow.Measurement.Pressure);
                                    lightList.AddFirst(nextDbRow.Measurement.Light);
                                    lastTimestamp = nextTimestamp;

                                    rowsInserted++;
                                }
                                break;
                        }

                        //Take the index for the last ID for the next comparision from the database. This way we iterate through the
                        //last elements of the table.
                        lastId = nextDbRow.Id;

                        //If the number of rows inserted in the linked lists is bigger than 30 we stop plotting data to avoid overload of
                        //data in the screen.
                        if (rowsInserted >= 30) break;

                        index++;
                    }
                }

                //When we are done of filling the linked list, we create a JsonObject (this is the kind of objects that AJAX charts 
                //understand) and share it with the Main.cshtml file.
                var result = new
                {
                    TemperatureValues = temperatureList,
                    TemperatureTimestampValues = timestampList,
                    HumidityValues = humidityList,
                    PressureValues = pressureList,
                    LightValues = lightList,
                    IsFahrenheit = changeToFahrenheit
                };

                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }

        //When the user clicks on Export data function this method is hit. It just read the FileFormat from the fileFormatModel in the view
        //and adapt the measurements to that particular format.
        [HttpPost]
        public ActionResult ExportData(FileFormatModel fileFormatModel)
        {
            //Read file export format
            string format = Request.Form["exportformat"];
            var exportFormat = fileFormatModel.formats;

            //Create string builder objects that will be filled with the measurements data from the database.
            string strDelimiter = "; ";
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter();
            using (var DbContext = new WeatherStationDatabaseContext())
            {
                //Read ALL the row from the database that correspond to the current Session ID.
                var mSessionID = Session["sessionID"];
                var rowsAffected = DbContext.devices_table.Where(x => x.SessionID == mSessionID.ToString()).OrderByDescending(x => x.Id).ToList();

                //Write the columns title first
                sw.Write("Timestamp" + strDelimiter);
                sw.Write("Name" + strDelimiter);
                sw.Write("SessionID" + strDelimiter);
                sw.Write("MAC Address" + strDelimiter);
                sw.Write("Temperature" + strDelimiter);
                sw.Write("Humidity" + strDelimiter);
                sw.Write("Pressure" + strDelimiter);
                sw.Write("Light");
                sw.Write("\r\n");

                //Proceed to iterate over all the measurements read from the database and store it in the StringBuilde object.
                foreach (RapidIoTDevice currentDevice in rowsAffected)
                {
                    sw.Write(currentDevice.Measurement.Timestamp + strDelimiter);
                    sw.Write(currentDevice.Name + strDelimiter);
                    sw.Write(currentDevice.SessionID + strDelimiter);
                    sw.Write(currentDevice.bleMACAddress + strDelimiter);
                    sw.Write(currentDevice.Measurement.Temperature + strDelimiter);
                    sw.Write(currentDevice.Measurement.Humidity + strDelimiter);
                    sw.Write(currentDevice.Measurement.Pressure + strDelimiter);
                    sw.Write(currentDevice.Measurement.Light);
                    sw.Write("\r\n");
                }
               
                //Set the file extension according to the user input in the dropdownlist.
                string filename = "RapidIoT_" + mSessionID;
                if (exportFormat!=null && exportFormat.Equals(".txt"))
                {
                    filename = filename + ".txt";
                }
                else 
                {
                    filename = filename + ".csv";
                }
            
                //Set the text format and add the current datetime to the title
                Response.ContentType = "text/plain";
                Response.AddHeader("content-disposition", "attachment;filename=" + string.Format(filename, string.Format("{0:ddMMyyyy}", DateTime.Today)));
                Response.Clear();

                //Store the data from the StringBuilder into the file and close it. The download will automatically be triggered in the
                //browser.
                using (StreamWriter writer = new StreamWriter(Response.OutputStream, Encoding.UTF8))
                {
                    writer.Write(sw.ToString());
                }
                Response.End();
                
            }
            return View();
        }

        //This method returns the temperature either in celsius or fahrenheit degrees if the convertToFahrenheit variable is set.
        private float convertTempIfNeeded(float inCelsius, bool convertToFahrenheit)
        {
            if(convertToFahrenheit)
            {
                return ((9 * inCelsius / 5) + 32);
            }
            else
            {
                return inCelsius;
            }
        }

    }
}